package com.garagebandhedgefund.maven;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.dataformat.avro.AvroAnnotationIntrospector;
import com.fasterxml.jackson.dataformat.avro.AvroFactory;
import com.fasterxml.jackson.dataformat.avro.AvroSchema;
import com.fasterxml.jackson.dataformat.avro.schema.AvroSchemaGenerator;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Generates Avro Schema files (*.avsc) from Java POJOs
 */
@Mojo( name = "generate", defaultPhase = LifecyclePhase.PROCESS_CLASSES, requiresDependencyCollection = ResolutionScope.COMPILE, requiresDependencyResolution = ResolutionScope.COMPILE,
        requiresProject = true)
public class PojoToAvscMojo extends AbstractMojo {

    @Component
    private MavenProject project;

    /**
     * Output of the Avro Schema files (.avsc)
     */
    @Parameter( defaultValue = "${project.build.outputDirectory}", property = "outputClassesDirectory", required = true)
    private File outputClassesDirectory;

    /**
     * Base package to scan for POJOs that will be converted into Avro Schema files
     */
    @Parameter( property = "basePackage", required = true)
    private String basePackage;

    public void execute() throws MojoExecutionException {
        getLog().info("Package name: " + this.basePackage);
        List<File> classFiles = this.getClassFiles(this.basePackage);
        for (File classFile: classFiles) {
            generateAvscFile(classFile);
        }
    }


    /**
     * Reads a Java class file and converts (writes) it to an Avro Schema file (.avsc)
     *
     * (NOTE: This method needs to be refactored to have less side effects. Maybe return the file instead of
     * writing it directly within the method.)
     *
     * @param classFile The .class file that will be converted to an Avro Schema file.
     * @throws MojoExecutionException
     */
    private void generateAvscFile(File classFile) throws MojoExecutionException{
        try {
            URLClassLoader classLoader = getProjectClassLoader();
            String className = extractClassName(classFile, classLoader);
            Class<?> clazz = classLoader.loadClass(className);
            for(Annotation annotation: clazz.getAnnotations()) {
                getLog().info("Annotation: " +annotation.toString());
            }

            ObjectMapper mapper = new ObjectMapper(new AvroFactory());
            mapper.setAnnotationIntrospectors(
                    new AvroAnnotationIntrospector(), new JacksonAnnotationIntrospector());
            AvroSchemaGenerator gen = new AvroSchemaGenerator();
            mapper.acceptJsonFormatVisitor(clazz, gen);
            AvroSchema schemaWrapper = gen.getGeneratedSchema();

            org.apache.avro.Schema avroSchema = schemaWrapper.getAvroSchema();
            String asJson = avroSchema.toString(true);
            getLog().info("Avro Schema: " + asJson);
            writeAvscFile(classFile, asJson);
        } catch (Exception e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    /**
     * This method determines the fully qualified Class name from an absolute File path.
     * For example, if the classFile is /myapp/target/classes/com/example/model/MyClass.class, this method
     * would compare the directory against the classLoader location (/myapp/target/classes)  and be able to then
     * determine the fully qualified Class name from there (ex:  com.example.model.MyClass )
     *
     * @param classFile File that points to the .class file of the Java Class
     * @param classLoader The classloader associated with the classFile
     * @return The fully qualified Class name.  For example:  com.example.model.MyClass
     * @throws MojoExecutionException
     */
    private String extractClassName(File classFile, URLClassLoader classLoader) throws MojoExecutionException {
        String className = null;

        URL classLoaderUrl = classLoader.getURLs()[0];
        String classLoaderPathString = classLoaderUrl.getPath();
        // Convert from a URL path to a directory path
        File classLoaderPath = null;
        try {
            classLoaderPath = new File(classLoaderUrl.toURI());
        } catch (URISyntaxException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        getLog().info("Classloader Path: " + classLoaderPath.getPath());

        String packageDirString = classFile.getPath().replace(classLoaderPath.getAbsolutePath(), "");
        getLog().info("packagedir: " + packageDirString);
        String packageName = packageDirString.replace(File.separator, "."); // Change directory slashes to dots
        packageName = packageName.replaceFirst(".", ""); // Remove the leading dot from the package name
        className = packageName.replaceAll("[.][^.]+$", ""); // Remove the .class extension
        getLog().info("className: " + className);
        return className;
    }

    /**
     * Returns a list of all classes within a package, as a list of class files
     *
     * This method uses the Maven classloader to locate the packages and classes
     *
     * @param basePackage Base package to scan for classes (ex: com.example.model)
     * @return Returns a list of .class files
     * @throws MojoExecutionException
     */
    private List<File> getClassFiles(String basePackage) throws MojoExecutionException  {
        List<File> classFiles = new ArrayList<File>();
        try {
            URLClassLoader loader = getProjectClassLoader();

            String basePackagePath = basePackage.replace(".", "/");
            Enumeration<URL> resources = loader.getResources(basePackagePath);
            while(resources.hasMoreElements()) {
                URL url = resources.nextElement();
                getLog().info("Base package classpath: " + url.getPath());
                File scannedDir = new File(url.getFile());
                for (File classFile : scannedDir.listFiles()) {
                    getLog().info("class files: " + classFile.getPath());
                    classFiles.add(classFile);
                }
            }
        } catch (IOException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        return classFiles;
    }

    /**
     * Returns the URLClassLoader from the Maven project
     * @return The URLClassLoader from the Maven project
     * @throws MojoExecutionException
     */
    private URLClassLoader getProjectClassLoader() throws MojoExecutionException{
        URLClassLoader loader = null;
        try {
            List<String> classpathElements = project.getCompileClasspathElements();
            List<URL> projectClasspathList = new ArrayList<URL>();
            for (String element : classpathElements) {
                getLog().info("Maven Project Classpath: " + element);
                try {
                    projectClasspathList.add(new File(element).toURI().toURL());
                } catch (MalformedURLException e) {
                    throw new MojoExecutionException(element + " is an invalid classpath element", e);
                }
            }

            loader = new URLClassLoader(projectClasspathList.toArray(new URL[projectClasspathList.size()]),
                    Thread.currentThread().getContextClassLoader());
        } catch (DependencyResolutionRequiredException e) {
            new MojoExecutionException("Dependency resolution failed", e);
        }
        return loader;
    }

    /**
     * Writes out the Avro Schema File
     * @param classFile Class file of the Java POJO used to generate the Schema
     * @param avscJson The JSON contents of the Avro Schema file
     * @throws MojoExecutionException
     */
    private void writeAvscFile(File classFile, String avscJson) throws MojoExecutionException {
        File outDir = outputClassesDirectory;

        if ( !outDir.exists() ) {
            outDir.mkdirs();
        }
        String avscFilename = classFile.getName().replace(".class", ".avsc");
        File avscFile = new File( outDir, avscFilename );

        FileWriter w = null;
        try {
            w = new FileWriter( avscFile );
            w.write(avscJson);
        }
        catch ( IOException e ) {
            throw new MojoExecutionException( "Error creating file " + avscFilename, e );
        } finally {
            if ( w != null ) {
                try  {
                    w.close();
                } catch ( IOException e ) {
                    // ignore
                }
            }
        }
    }
}
