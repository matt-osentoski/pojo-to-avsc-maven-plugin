## POJO to Avro Schema file (avsc) Creator - Maven Plugin
This project is Maven plugin that converts java classes into Avro Schema files (avsc).
The purpose of this project is to create a 'code first' approach. Java code is the
source of truth following this pattern and Avro schemas are updated from the code
builds. In this scenario Avro is mostly a transportation mechanism for streaming and big data
systems.  This is backwards from typical scenarios where Avro would be the source
of truth and generate the Java classes.  This plugin is useful for legacy systems that require
Avro transport, but aren't ready to commit to Avro as the source of truth.

## Repository
https://bitbucket.org/matt-osentoski/pojo-to-avsc-maven-plugin

## Build
mvn clean install

## Usage
Add the following `build` section to your pom.xml file:
```xml
<build>
    <plugins>
      <plugin>
        <groupId>com.garagebandhedgefund.maven</groupId>
        <artifactId>pojo-to-avsc-maven-plugin</artifactId>
        <version>1.0-SNAPSHOT</version>
        <configuration>
          <basePackage>com.example.model</basePackage>
          <!-- The element below shows the default value and can be excluded -->
          <outputClassesDirectory>${project.build.outputDirectory}</outputClassesDirectory>
        </configuration>
        <!-- This section will run the 'generate' goal automatically during the 'compile' phase -->
        <executions>
          <execution>
            <phase>compile</phase>
            <goals>
              <goal>generate</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
```

## Manually Run the Maven goal
To manually run the Maven goal, use the following command:
```java
mvn com.garagebandhedgefund.maven:pojo-to-avsc-maven-plugin:generate

```
